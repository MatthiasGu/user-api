var express = require('express');
var router = express.Router();
var userController = require('../controllers/user.controller');
/* GET home page. */

module.exports = function(app) {
    app.post('/users/create', userController.createUser);
    app.get('/users/get/:email', userController.retrieveUser);
    app.put('/users/update', userController.updateUser);
    app.delete('/users/delete', userController.deleteUser);

};
