/**
 * Created by mgudenas on 11/06/2017.
 */
const express = require('express');
const app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var db;

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

require('./routes/index')(app);
const PORT = process.env.PORT || 3000;

app.get('/', function(req, res) {
    res.send('Welcome!\n');
});

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}

app.use(allowCrossDomain);

db = mongoose.connect('mongodb://mgudenas:123123@ds141358.mlab.com:41358/mg-website');

app.listen(PORT, (error) => {
    if (error) throw (error);
console.info(`==> Server started on ${PORT}`);
});
