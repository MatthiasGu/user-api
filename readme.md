## How to use the API ##

The API has no front end.

1. Pull the repository.
2. In your terminal, navigate to the root of the project.
3. Run ``` npm install ``` in the terminal.
4. Run ``` node server.js ``` in the terminal.
5. Open Postman (or a similar tool)
6. Refer to the API reference below.

## API reference ##

The host is localhost:3000/. You can then append the API endpoint to the host.

For example, endpoint users/create becomes localhost:3000/users/create.


### POST users/create ###

Creates a user with the given details. The e-mail is unique, so no duplicates will be accepted.


For example:
```
POST /users/create HTTP/1.1
Host: localhost:3000
Accept: application/json
Content-Type: application/json

{
    "email": "test@gmail.com",
    "forename": "test",
    "surname": "test"
}
```

### GET users/get/[email] ###

Returns the user with the given [email] if one exists. 

For example:

```
GET /users/get/test@gmail.com
Host: localhost:3000
```

### PUT users/update ###

Finds the user with the given email and updates the forename and surname with the ones provided in the
request. Returns the updated user. 

For example:

```
PUT /users/update HTTP/1.1
Host: localhost:3000
Accept: application/json
Content-Type: application/json

{
    "email": "test@gmail.com",
    "forename": "mjg",
    "surname": "test"
}
```

### DELETE users/delete ###

Finds the user with the given email, deletes it. If successful, returns the deleted user.

For example:

```
DELETE /users/update HTTP/1.1
Host: localhost:3000
Accept: application/json
Content-Type: application/json

{
    "email": "test@gmail.com"
}
```
