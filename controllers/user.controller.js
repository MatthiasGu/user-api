/**
 * Created by mgudenas on 11/06/2017.
 */

var User = require('../models/user');

module.exports = {
    createUser: function(req, res) {
        var data = req.body;
        var created = new Date();
        User.create({
            email: data.email,
            forename: data.forename,
            surname: data.surname,
            created
        }, function(err, user) {
            if (err) {
                console.log('Error: cannot create new user');
                res.send(err);
            } else {
                res.send(user);
            }
        });
    },

    retrieveUser: function(req, res) {
        var conditions = { email: req.params.email };
        User.find(conditions).exec().then(function(result) {
            if (result.length == 0) {
                res.send("Cannot find a user with the given e-mail.");
            } else {
                res.send(result);
            }
        })
    },

    updateUser: function(req, res) {
        var conditions = { email: req.body.email };
        var update = {
            $set: {
                forename: req.body.forename,
                surname: req.body.surname
            }
        };
        User.findOneAndUpdate(conditions, update, function(err, user) {
            if (err) {
                res.send(err);
            } else {
                if (user) {
                    User.find({email: user.email}).exec().then(function(result) {
                        res.send(result);
                    });

                } else {
                    res.send("Cannot find a user with the given e-mail.")
                }
            }
        })
    },

    deleteUser: function(req, res) {
        var conditions = { email: req.body.email };
        User.findOneAndRemove(conditions, function(err, user) {
            if (err) {
                res.send(err);
            } else {
                if (user) {
                    res.send(user);
                } else {
                    res.send("Cannot find a user with the given e-mail.")
                }
            }
        });


    }


};