/**
 * Created by mgudenas on 11/06/2017.
 */

var mongoose = require('mongoose');

var User = require('./user');

var userData = {
    email: "test@gmail.com",
    forename: "test",
    surname: "test"
};

var self = module.exports = function(done) {
    User.create(userData, function(err, user) {
        if(err) {
            console.log('Error creating new user: ',err);
            return done(err);

        } else {
            if (user) {
                return done();
            }
        }
    })

};

self(function()
{

});
