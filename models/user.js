/**
 * Created by mgudenas on 11/06/2017.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserModel = new Schema({
    email: {
        type: String,
        default: '',
        unique: true,
        required: true
    },
    forename: {
        type: String,
        default: '',
        required: true
    },
    surname: {
        type: String,
        default: '',
        required: true
    },
    created: {
        type: Date,
        required: false
    }

}, {
    collection: 'users'
});

module.exports = mongoose.model('User', UserModel);